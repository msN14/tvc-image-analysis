# TVC Image Analysis

The high speed chemiluminescence images are analyzed using Python packages. The ASCII text file is the raw data files.

1. conversion of txt file to npy file
2. merge all the npy files at each time instant to single npy file
3. calculate mean and std from the 3D numpy array

The code uses numpy module for the numerical calculations.
