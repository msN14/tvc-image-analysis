#%% SECTION 1
#----------------------------------------------[Imports]-----------------------
import numpy as np
import glob
import os
#------------------------------------------------------------------------------

#----------------------------------------------[Create Folder]-----------------
if os.path.isdir('./NPY Results') == False:
    os.mkdir('./NPY Results')
#------------------------------------------------------------------------------

# creating memory mapped .npy file
qt = np.lib.format.open_memmap('./NPY Results/Qt.npy', dtype='int32', 
                               mode='w+', shape=(10000,750,850))

#----------------------------------------------[Merge .npy files]--------------
files = glob.iglob('./NPY/*.npy')
F = []
for f in files:
    F.append(f)
    
i = 0
for ipath in F:
    print("path = ", ipath)
    d = np.load(ipath)
    qt[i,:,:] = d
    i = i + 1

qt.flush()
#------------------------------------------------------------------------------