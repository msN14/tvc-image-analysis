#%% SECTION 1
#----------------------------------------------[Imports]-----------------------
import numpy as np
import glob
import os
#------------------------------------------------------------------------------

#----------------------------------------------[Create Folder]-----------------
if os.path.isdir('./NPY') == False:
    os.mkdir('./NPY')
#------------------------------------------------------------------------------

# Get x and y cut from geometry images
# orientation is same as experiments

x1, x2 = 200, 950   # x clipping
y1, y2 = 50, 900    # y clipping

#----------------------------------------------[Read Background Signal]--------
bg = np.load('./Background/NPY Results/background-mean-oh.npy')
#------------------------------------------------------------------------------

#----------------------------------------------[ASCII .txt >> .npy]------------
from multiprocessing import Pool

files = glob.iglob('./ASCII TEXT/*.txt')
F = []
for f in files:
    F.append(f)
    
def creat_process_npy(path):
    qm = np.genfromtxt(path, dtype=None)  
    with open('./NPY/CR'+path[14:-4]+'.npy', 'wb') as f: # C=Clipped + R=Rotate
        cp = qm[y1:y2, x1:x2] - bg                # remove background noise
        cp[cp < 0] = 0                            # remove all negative counts
        cp[0:400,300:] = 0                        # remove counts from walls
        cp = cp.astype(int)                       # change to int elements
        np.save(f, np.rot90(cp, k=1, axes=(1,0))) # rotate and save
    return

if __name__ == '__main__':
    with Pool(24) as p:
        # print(p.map(creat_process_npy, F))
        p.map(creat_process_npy, F)
#------------------------------------------------------------------------------
